package logic;

import java.util.ArrayList;

import models.Arco;

public class GrafoPonderado 
{
	
	private int[][] _grafo;
	private ArrayList<Arco> _arcos;
	private int _peso_total;
	
	public GrafoPonderado(int vertices)
	{
		verificarVerticeConstructor(vertices);
		_grafo = new int[vertices][vertices];
		_arcos = new ArrayList<Arco>();
		_peso_total = 0;
	}
	
	public void agergarVertice()
	{
		int[][] nuevo = new int[obtenerTamanio()+1][obtenerTamanio()+1];
		for(int i = 0; i < obtenerTamanio(); i++)
		{
			for(int j = 0; j < obtenerTamanio(); j++)
			{
				nuevo[i][j] = obtenerPeso(i,j);
			}
		}
		_grafo = nuevo;
	}
	
	public void eliminarVertice(int vertice)
	{
		int[][] nuevo = new int[obtenerTamanio()-1][obtenerTamanio()-1];
		for(int i = 0; i < nuevo.length; i++)
		{
			for(int j = 0; j < nuevo.length; j++)
			{
				if(i != vertice && j != vertice)
				{
					nuevo[i][j] = obtenerPeso(i,j);
				}
			}
		}
		_grafo = nuevo;
		eliminarArco(vertice);
	}
	
	public void agregarArco(int posicionX, int posicionY, int peso)
	{
		Arco nuevo = new Arco(posicionX, posicionY, peso);
		
		for(Arco arco : _arcos) {
			if(arco.esSimetrico(nuevo));
			break;
		}
		
		verificarPeso(peso);
		verificarPosiciones(posicionX, posicionY);
		verificarBucle(posicionX, posicionY);
		_grafo[posicionX][posicionY] = peso;
		_grafo[posicionY][posicionX] = peso;
		_arcos.add(new Arco(posicionX, posicionY, peso));
	}
	
	public void eliminarArcos() {
		this._arcos = new ArrayList<Arco>();
	}
	
	public void eliminarArco(int vertice)
	{
		for(int i = 0; i < _arcos.size(); i++)
		{
			if(_arcos.get(i).obtenerPosicionX() == vertice || _arcos.get(i).obtenerPosicionY() == vertice)
			{
				_arcos.remove(i);
			}
		}
	}
	
	public int obtenerPeso(int posicionX, int posicionY)
	{
		return _grafo[posicionX][posicionY];
	}
	
	public ArrayList<Arco> obtenerArcos()
	{
		return _arcos;
	}
	
	private void verificarVerticeConstructor(int vertice)
	{
		if(vertice < 0)
		{
			throw new IllegalArgumentException("La cantidad de vertices no puede ser negativa");
		}
	}
	
	private void verificarVertice(int vertice)
	{
		if(vertice > obtenerTamanio()-1)
		{
			throw new IllegalArgumentException("El vertice especificado no existe." + vertice);
		}
	}
	
	private void verificarPosiciones(int posicionX, int posicionY)
	{
		if(posicionX < 0 || posicionX > obtenerTamanio()-1)
		{
			throw new IndexOutOfBoundsException("El vertice " + posicionX + " no existe");
		}
		if(posicionY < 0 || posicionY > obtenerTamanio()-1)
		{
			throw new IndexOutOfBoundsException("El vertice " + posicionY + " no existe");
		}
		
	}
	
	private void verificarBucle(int posicionX, int posicionY)
	{
		if(posicionX == posicionY)
		{
			throw new IllegalArgumentException("No son validos los bucles en los vertices.");
		}
	}
	
	private void calcularPesoTotal()
	{
		for(Arco a: _arcos)
		{
			_peso_total+= a.obtenerPeso();
		}
	}
	
	private void verificarPeso(int peso)
	{
		if(peso < 0)
		{
			throw new IllegalArgumentException("No se le puede aplicar pesos negativos a las aristas.");
		}
	}
	
	public int[] obtenerVecinos(int vertice)
	{
		verificarVerticeConstructor(vertice);
		verificarVertice(vertice);
		return _grafo[vertice];
	}
	
	public int getPesoTotal()
	{
		calcularPesoTotal();
		return _peso_total;
	}
	
	public double obtenerCostoPromedio()
	{
		return (getPesoTotal()/(_arcos.size()/1.0));
	}
	
	public int obtenerTamanio()
	{
		return _grafo.length;
	}
	
	@Override
	public String toString()
	{
		StringBuilder ret = new StringBuilder();
		
		for(int i = 0; i < obtenerTamanio(); i++)
		{
			for(int j = i; j < obtenerTamanio(); j++)
			{
				int distancia = obtenerPeso(i,j);
				if(distancia != 0)
				{
					ret.append("(" + (i) + ", " + (j) + ", "+ obtenerPeso(i,j) +")");
				}
			}
		}
		
		return ret.toString();
	}
}
