package logic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DesviacionEstandarTest {
	
	private GrafoPonderado _grafo;
	
	@Test
	void desviacionStandarTest() 
	{
		_grafo = new GrafoPonderado(4);
		_grafo.agregarArco(0, 1, 5);
		_grafo.agregarArco(0, 2, 1);
		_grafo.agregarArco(1, 3, 150);
		_grafo.agregarArco(2, 3, 6);

		assertTrue(DesviacionEstandar.calcular(_grafo) == 63.24752959602454);
	}
}
