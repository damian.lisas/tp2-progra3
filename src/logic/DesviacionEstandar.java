package logic;

import java.util.ArrayList;

import models.Arco;

public class DesviacionEstandar 
{	
	public static double calcular(GrafoPonderado grafo)
	{
		double media = grafo.obtenerCostoPromedio();
		ArrayList<Arco> arcos = grafo.obtenerArcos();
		
		double sumatoria = 0;
		for(int i = 0; i < arcos.size(); i++)
		{
			sumatoria += Math.pow(Math.abs(media-arcos.get(i).obtenerPeso()), 2);
		}
		double division = sumatoria/arcos.size();
		
		return Math.sqrt(division);
	}
}
