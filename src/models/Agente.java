package models;

import java.io.Serializable;
import java.util.UUID;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Agente implements Serializable, Comparable<Agente>
{
	
	/**
	 * Auto-Generado
	 */
	private static final long serialVersionUID = -1494892985614264398L;
	
	private UUID _uuid;
	private String _nombre_clave;
	private Coordinate _ubicacion;
	private int _numero_de_agente;
	
	public Agente(String nombreClave, Coordinate ubicacion, int numeroDeAgente)
	{
		this._uuid = UUID.randomUUID();
		verificarArgumentos(nombreClave, ubicacion, numeroDeAgente);
		_nombre_clave = nombreClave;
		_ubicacion = ubicacion;
		_numero_de_agente = numeroDeAgente;
	}
	
	private void verificarArgumentos(String nombreClave, Coordinate ubicacion, int numeroDeAgente)
	{
		if(nombreClave == null)
		{
			throw new NullPointerException("El nombre del agente debe ser un objeto valido!");
		}
		if(nombreClave.length() == 0)
		{
			throw new IllegalArgumentException("El agente debe tener un nombre clave.");
		}
		if(ubicacion == null)
		{
			throw new NullPointerException("El agente debe tener una ubicacion valida.");
		}
		if(numeroDeAgente < 0)
		{
			throw new IndexOutOfBoundsException("El numero de agente debe ser mayor o igual a 0");
		}
	}
	
	public UUID getUuid() {
		return this._uuid;
	}
	
	public String obtenerNombre()
	{
		return _nombre_clave;
	}
	
	public void setNombre(String nombre) {
		this._nombre_clave = nombre;
	}
	
	public Coordinate obtenerubicacion()
	{
		return _ubicacion;
	}
	
	public void setUbicacion(Coordinate ubicacion) {
		this._ubicacion = ubicacion;
	}
	
	public int obtenerNumeroDeAgente()
	{
		return _numero_de_agente;
	}
	
	public void setNumeroDeAgente(int numeroDeAgente) {
		this._numero_de_agente = numeroDeAgente;
	}
	
	@Override
	public String toString()
	{
		return "Nombre clave : " + _nombre_clave + "\n" + 
				"Ubicacion: " + _ubicacion.toString() + "\n" + 
				"Numero de agente: " + _numero_de_agente + "\n" +
				"Uid: " + _uuid + "\n";
	}

	/**
	 * Compara cada uno de los campos de este agente contra los campos del otro agente, si todos los campos son iguales retorna 0, sino retorna 1
	 * 
	 * @param other Otro agente
	 * @return {@literal 0} si los agentes son iguales
	 * @return {@literal 1} si alguno de los campos de los agentes no son iguales
	 */
	@Override
	public int compareTo(Agente other) {
		boolean sonIguales = true;
		
		sonIguales = sonIguales && this._uuid.equals(other.getUuid());
		sonIguales = sonIguales && this._nombre_clave.equals(other.obtenerNombre());
		sonIguales = sonIguales && this._ubicacion.equals(other.obtenerubicacion());
		sonIguales = sonIguales && this._numero_de_agente == other._numero_de_agente;
		
		if(sonIguales == true) return 0;
		else return 1;
	}
}
