package models;

public class Arco
{
	private int _posicion_x;
	private int _posicion_y;
	private int _peso;
	
	public Arco(int posicionX, int posicionY, int peso)
	{
		_posicion_x = posicionX;
		_posicion_y = posicionY;
		_peso = peso;
	}
	
	public int obtenerPosicionX()
	{
		return _posicion_x;
	}
	
	public int obtenerPosicionY()
	{
		return _posicion_y;
	}
	
	public int obtenerPeso()
	{
		return _peso;
	}
	
	/**
	 * Verifica si el otro arco es simetrico con este arco<br>
	 * 2 arcos son simetricos cuando dados 2 arcos A, B, origen A = destino B y destino de A = origen de B y peso A = peso B
	 * @param otro Otro Arco
	 * @return {@literal true} si los arcos son simetricos, {@literal false} caso contrario
	 */
	public boolean esSimetrico(Arco otro) {
		boolean esSimetrico = true;
		
		esSimetrico = esSimetrico && _posicion_x == otro._posicion_y;
		esSimetrico = esSimetrico && _posicion_y == otro._posicion_x;
		esSimetrico = esSimetrico && _peso == otro._peso;
		
		return esSimetrico;
	}
	
	@Override
	public String toString()
	{
		return "(" + _posicion_x + ", " + _posicion_y + ", "+ _peso + ")";
	}
}
