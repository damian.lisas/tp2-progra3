package controllers;

import java.util.ArrayList;
import java.util.List;

import org.openstreetmap.gui.jmapviewer.Coordinate;

import logic.DesviacionEstandar;
import logic.GrafoPonderado;
import logic.Haversine;
import logic.Primm;
import models.Agente;
import models.Arco;
import persistence.PersistenceFacade;

//TODO: falta agregar los metodos que devuelvan las estadiastas que agregaste a grafoPondereado
public class MainController 
{
	private GrafoPonderado _grafo;
	private ArrayList<Agente> _agentesEnMapa;
	private ArrayList<Agente> _agentesConocidos;
	private PersistenceFacade fachada;
	
	public MainController()
	{
		fachada = new PersistenceFacade();
		
		_grafo = new GrafoPonderado(0);
		_agentesEnMapa = new ArrayList<Agente>();
		_agentesConocidos = (ArrayList<Agente>) fachada.obtenerTodosLosAgentes();
	}
	
	public void agregarAgenteConocido(Agente agente)
	{
		agente.setNumeroDeAgente(_agentesEnMapa.size());
		
		if(_agentesEnMapa.contains(agente) == false) {
			_agentesEnMapa.add(agente);
			_grafo.agergarVertice();
			actualizarGrafo();
		}
		
	}
	
	public Agente agregarNuevoAgente(String nombre, Coordinate ubicacion)
	{
		Agente nuevo = new Agente(nombre, ubicacion, _agentesEnMapa.size());
		
		_agentesConocidos.add(nuevo);
		_agentesEnMapa.add(nuevo);
		_grafo.agergarVertice();
		
		actualizarGrafo();
		
		fachada.guardarAgente(nuevo);
		
		return nuevo;
	}

	/**
	 * Elimina el agente del mapa
	 * @param agente Agente a eliminar del mapa
	 */
	public void eliminarAgente(Agente agente)
	{
		if(_agentesEnMapa.contains(agente)) {
			
			for(int i = agente.obtenerNumeroDeAgente() + 1; i < _agentesEnMapa.size(); i++) {
				_agentesEnMapa.get(i).setNumeroDeAgente(i - 1);
			}
		
			_agentesEnMapa.remove(agente);
			
			_grafo.eliminarVertice(agente.obtenerNumeroDeAgente());
			actualizarGrafo();
		}
	}

	/**
	 * Elimina el agente y lo borra de la base de datos
	 * @param agente Agente a eliminar
	 */
	public void olvidarAgente(Agente agente) {
		
		this.eliminarAgente(agente);
		
		this._agentesConocidos.remove(agente);
		
		this.fachada.eliminarAgente(agente);
	}

	public void eliminarTodosLosAgentes() {
		this.fachada.elimarTodos();
	}
	
	public Agente obtenerAgente(int posicion)
	{
		return _agentesEnMapa.get(posicion);
	}
	
	public List<Agente> obtenerAgentesConocidos() {
		return this._agentesConocidos;
	}
	

	public List<Agente> obtenerAgentesEnMapa() {
		return this._agentesEnMapa;
	}
	
	public Agente obtenerUltimoAgente()
	{
		return _agentesEnMapa.get(_agentesEnMapa.size()-1);
	}
	
	public void generarAgm()
	{
		_grafo = Primm.generarAgm(_grafo);
	}
	
	public ArrayList<Arco> obtenerArcos()
	{
		return _grafo.obtenerArcos();
	}
	
	/**
	 * Genera el grafo asumiendo que se agrego un vertice nuevo
	 */
	private void generarGrafoCompleto()
	{
		for(int i = 0; i < _agentesEnMapa.size()-1; i++)
		{
			Coordinate ubicacionOrigen = _agentesEnMapa.get(i).obtenerubicacion();
			Coordinate ubicacionDestino = _agentesEnMapa.get(_agentesEnMapa.size()-1).obtenerubicacion();
			int distancia = (int) Haversine.obtenerDistanciaEnKm(ubicacionOrigen, ubicacionDestino);
			_grafo.agregarArco(i, (_agentesEnMapa.size()-1), distancia);
		}
	}
	
	/**
	 * Elimina todos los arcos y genera un grafo completo nuevamente
	 */
	private void actualizarGrafo() {
		_grafo.eliminarArcos();
		
		for(int i = 0; i < _agentesEnMapa.size() - 1; i++)
		{
			for(int j = i + 1; j < _agentesEnMapa.size(); j++)
			{
				Coordinate ubicacionOrigen = _agentesEnMapa.get(i).obtenerubicacion();
				Coordinate ubicacionDestino = _agentesEnMapa.get(j).obtenerubicacion();
				int distancia = (int) Haversine.obtenerDistanciaEnKm(ubicacionOrigen, ubicacionDestino);
				_grafo.agregarArco(i, j, distancia);
			}
		}
	}
	
	public void setFachada(PersistenceFacade fachada) {
		this.fachada = fachada;
	}
	
	public void actualizarAgentesConocidos() {
		this._agentesConocidos = (ArrayList<Agente>) this.fachada.obtenerTodosLosAgentes();
	}

	public static MainController createTestController() {
		MainController controller = new MainController();
		controller.setFachada(PersistenceFacade.createTestFacade());
		controller.actualizarAgentesConocidos();
		
		return controller;
	}
	
	public double obtenerCostoPromedio()
	{
		return _grafo.obtenerCostoPromedio();
	}
	
	public int obtenerPesoTotal()
	{
		return _grafo.getPesoTotal();
	}
	
	public double obtenerDesviacionEstandar()
	{
		return DesviacionEstandar.calcular(_grafo);
	}
}
