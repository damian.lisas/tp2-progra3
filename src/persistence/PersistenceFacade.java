package persistence;

import java.util.List;
import java.util.UUID;

import models.Agente;
import persistence.dao.AgenteDao;

public class PersistenceFacade {

	private AgenteDao dao;
	
	public PersistenceFacade() {
		this.dao = new AgenteDao();
	}
	
	public PersistenceFacade(String path, String extension) {
		this.dao = new AgenteDao(path, extension);
	}
	
	public boolean guardarAgente(Agente agente) {
		return this.dao.save(agente);
	}
	
	public Agente obtenerAgente(UUID agenteUid) {
		return this.dao.get(agenteUid);
	}
	
	public List<Agente> obtenerTodosLosAgentes() {
		return this.dao.getAll();
	}
	
	public boolean actualizarAgente(Agente agente) {
		return this.dao.update(agente);
	}
	
	public boolean eliminarAgente(Agente agente) {
		return this.dao.delete(agente);
	}

	public boolean elimarTodos() {
		return this.dao.deleteAll();
	}
	
	public static PersistenceFacade createTestFacade() {
		PersistenceFacade facade = new PersistenceFacade("database/test/", ".txt");
		
		return facade;
	}
}
