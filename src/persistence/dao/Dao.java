package persistence.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Data Access Object para el manejo de entidades serializables.<br>
 * Es abstracta para que no pueda ser instanciada
 * 
 * @author Damian
 *
 * @param <T>
 */
public abstract class Dao <T extends Serializable> {
	
	protected static final String DEFAULT_PATH = "database/";
	protected static final String DEFAULT_EXTENSION = ".txt";

	/**
	 * Directorio donde se van a guardar los archivos creados
	 */
	protected String basePath;
	
	/**
	 * Extension que tendran los archivos que se guarden
	 */
	protected String extension;
	
	/**
	 * Usada para determinar el directorio donde se van a ubicar las entidades que se guarden y la extension que tendran las mismas<br>
	 * Tambien se debe crear el directorio si es que no existe
	 * 
	 * @param path Path del directorio
	 * @param extension Extension que se le data a los archivos
	 */
	protected abstract void configure(String path, String extension);
	
	/**
	 * Guarda la Entidad dada como parametro
	 * @param entity Entidad a grabar
	 * @param uid Unique ID de la entidad
	 * @return true si la entidad se graba exitosamente, sino retorna false
	 */
	protected boolean save(T entity, UUID uid) {
		try {
			FileOutputStream fos = new FileOutputStream(this.basePath + uid.toString() + this.extension);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(entity);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/**
	 * Obtiene la por su UUID
	 * @param uid UUID de la entidad
	 * @return T Entidad encontrada
	 * @return Null si no se encuentara el archivo indicado
	 */
	@SuppressWarnings({ "unchecked", "resource" })
	protected T get(UUID uid) {
		File[] files = new File(basePath).listFiles();
		T retorno = null;
		
		for(File file : files) {
			try {
				if(file.isDirectory() == false) {
					if(file.getName().equals(uid + extension)) {
						FileInputStream fis = new FileInputStream(file);
						ObjectInputStream in = new ObjectInputStream(fis);
						retorno = (T) in.readObject();
						in.close();
						break;
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		return retorno;
	}
	
	/**
	 * Obtiene todas las entidades guardadas
	 * @return List<T> la lista de entidades
	 */
	@SuppressWarnings({ "unchecked", "resource" })
	protected List<T> getAll() {
		File[] files = new File(basePath).listFiles();
		List<T> retorno = new ArrayList<T>();
		
		for(File file : files) {
			try {
				if(file.isDirectory() == false) {
					FileInputStream fis = new FileInputStream(file);
					ObjectInputStream in = new ObjectInputStream(fis);
					retorno.add((T) in.readObject());
					in.close();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
			
		}
		
		return retorno;
	}
	
	/**
	 * Elimina el archivo indicado
	 * @param filePath Path absoluto del archivo
	 * @return true si se elimina correctamente, sino retorna false
	 */
	protected boolean delete(UUID uid) {
		String filePath;
		
		try {
			filePath = new File(this.basePath + uid + this.extension).getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		File file = new File(filePath);
		
		if(file.exists())
			return file.delete();
		
		return false;
	}

}
