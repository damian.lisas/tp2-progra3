package persistence.dao;

import java.io.File;
import java.util.List;
import java.util.UUID;

import models.Agente;

public class AgenteDao extends Dao<Agente> {
	
	public AgenteDao() {
		this.configure(Dao.DEFAULT_PATH, Dao.DEFAULT_EXTENSION);
	}
	
	public AgenteDao(String path, String extension) {
		this.configure(path, extension);
	}

	/**
	 * Guarda el agente siempre y cuando no exista un agente no exista un agente con el mismo uid ya guardado
	 * @param agente Agente a guardar
	 * @return {@literal true} si el agente es guardado exitosamente
	 * @return {@literal false} si no puede guardar el agente o ya existe
	 */
	public boolean save(Agente agente) {
		
		if(this.get(agente.getUuid()) != null) {
			return false;
		}
		
		return super.save(agente, agente.getUuid());
	}

	/**
	 * Obtiene el Agente a partir de su UID
	 * 
	 * @param uid UUID del agente
	 * @return Agente el agente encontrado
	 * @return {@literal null} si no encuentra el agente especificado
	 */
	@Override
	public Agente get(UUID uid) {
		return super.get(uid);
	}
	
	/**
	 * Obtiene todos los agentes guardados
	 * 
	 * @return List<Agente> lista de agentes
	 */
	@Override
	public List<Agente> getAll() {
		return super.getAll();
	}

	/**
	 * Actualiza los datos del agente indicado
	 * @param actual Agente a actualizar
	 * @return {@literal true} si el agente se actualiza correctamente, {@literal false} caso contratio
	 */
	public boolean update(Agente actual) {

		Agente nuevo = this.get(actual.getUuid());
		
		if(nuevo == null) {	return false; }

		//Realizo la actualizacion eliminando el archivo actual y creando uno nuevo
		boolean guardado = false;
		boolean eliminado = false;
		
		nuevo.setNombre(actual.obtenerNombre());
		nuevo.setUbicacion(actual.obtenerubicacion());
		nuevo.setNumeroDeAgente(actual.obtenerNumeroDeAgente());
		
		eliminado = this.delete(actual);
		guardado = this.save(nuevo);
		
		return guardado && eliminado;
	}

	/**
	 * Elimina el agente inidicado
	 * @param agente Agente a eliminar
	 * @return {@literal true} si el agente se elimina exitosamente, {@literal false} caso contrario
	 */
	public boolean delete(Agente agente) {
		return super.delete(agente.getUuid());
	}
	
	/**
	 * Elimina todos los agentes de la base de datos
	 * 
	 * @return {@literal true} si se pueden eliminar todos los agentes, {@literal false} caso contrario
	 */
	public boolean deleteAll() {
		List<Agente> agentes = this.getAll();
		boolean eliminados = true;
		
		for(Agente agente : agentes) {
			eliminados = eliminados && this.delete(agente);
		}
		
		return eliminados;
	}

	@Override
	protected void configure(String path, String extension) {
		super.basePath = path;
		super.extension = extension;
		
		new File(super.basePath).mkdir();
	}
}
