package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.PlainDocument;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;

import controllers.MainController;
import models.Agente;
import models.Arco;

public class Interfaz 
{
	private JFrame _frame;
	private JMapViewer _mapa;
	private JButton _generar_red;
	private MainController _controller;
	private JPanel agentesConocidos;
	private JTextField _titulo_estadisticas;
	private JTextField _peso_total;
	private JTextField _costo_promedio;
	private JTextField _desviacion_estandar;

	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			@Override
			public void run() 
			{
				try 
				{
					Interfaz window = new Interfaz();
					window._frame.setVisible(true);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	public Interfaz() 
	{
		initialize();
	}
	
	private void initialize() 
	{
		System.out.println("Creando interfaz");
		_controller = new MainController();

		inicializarFrame();
		iniciliazarMapa();
		incializarBotonGenerarRed();
		inicializarAgentesConcidos();
		inicializarEstadisticas();
	}

	private void inicializarEstadisticas() 
	{
		_titulo_estadisticas = new JTextField("Estadisticas ");
		_titulo_estadisticas.setBounds(1050, 25, 100, 20);
		_titulo_estadisticas.setEditable(false);
		_titulo_estadisticas.setBorder(null);
		_titulo_estadisticas.setFont(new Font("Dialog", Font.BOLD, 12));
		_frame.add(_titulo_estadisticas);
		
		_costo_promedio = new JTextField("Costo promedio ");
		_costo_promedio.setBounds(950, 75, 300, 20);
		_costo_promedio.setEditable(false);
		_costo_promedio.setBorder(null);
		_costo_promedio.setFont(new Font("Dialog", Font.BOLD, 12));
		_frame.add(_costo_promedio);
		
		_peso_total = new JTextField("Peso Total ");
		_peso_total.setBounds(950, 55, 300, 20);
		_peso_total.setEditable(false);
		_peso_total.setBorder(null);
		_peso_total.setFont(new Font("Dialog", Font.BOLD, 12));
		_frame.add(_peso_total);
		
		_desviacion_estandar = new JTextField("Desviacion estandar ");
		_desviacion_estandar.setBounds(950, 95, 300, 20);
		_desviacion_estandar.setEditable(false);
		_desviacion_estandar.setBorder(null);
		_desviacion_estandar.setFont(new Font("Dialog", Font.BOLD, 12));
		_frame.add(_desviacion_estandar);
	}
	
	private void inicializarFrame()
	{
		_frame = new JFrame();
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_frame.setBounds(0, 0, 1300, 700);
		_frame.setLocationRelativeTo(null);
		_frame.getContentPane().setLayout(null);
	}
	
	private void iniciliazarMapa() 
	{
		_mapa = new JMapViewer();
		_mapa.setBounds(0, 0, 400, 700);
		_mapa.setZoomContolsVisible(false);
		_mapa.setDisplayPositionByLatLon( -40.8134499, -62.9966812, 4);
		_mapa.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				if (e.getButton() == MouseEvent.BUTTON1) 
				{
					Coordinate ubicacion = _mapa.getPosition(e.getPoint());
					agregarAgente(ubicacion);
					ocultarEstadisticas();
				}
			}
		});
		_mapa.addMouseWheelListener(new MouseWheelListener() 
		{
			@Override
			public void mouseWheelMoved(MouseWheelEvent arg0) 
			{
				_mapa.setDisplayPositionByLatLon( -40.8134499, -62.9966812, 4); 
			}
		});
		_frame.getContentPane().add(_mapa);
	}
	
	private void incializarBotonGenerarRed()
	{
		_generar_red = new JButton("Generar red");
		_generar_red.setBounds(425, 10, 150, 20);
		_generar_red.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				_controller.generarAgm();
				_mapa.removeAllMapPolygons();
				mostrarArcos();
				actulizarEstadisticas();
			}
		});
		_frame.getContentPane().add(_generar_red);
	}
	
	private void inicializarAgentesConcidos() {
		JLabel lblAgentesConocidos = new JLabel("Agentes Conocidos");
		lblAgentesConocidos.setBounds(620, 30, 200, 14);
		_frame.getContentPane().add(lblAgentesConocidos);
		
		agentesConocidos = new JPanel();
		agentesConocidos.setLayout(new GridLayout(1 + _controller.obtenerAgentesConocidos().size(), 6));
		agentesConocidos.setBounds(425, 50, 500, 20 + 20 * _controller.obtenerAgentesConocidos().size());
		agentesConocidos.setVisible(true);
		
		agentesConocidos.add(new JLabel("Nombre"));
		agentesConocidos.add(new JLabel("Latitud"));
		agentesConocidos.add(new JLabel("Longitud"));
		agentesConocidos.add(new JLabel(""));
		agentesConocidos.add(new JLabel(""));
		agentesConocidos.add(new JLabel(""));
		for(Agente agente : _controller.obtenerAgentesConocidos()) {
			agentesConocidos.add(new JLabel(agente.obtenerNombre()));
			agentesConocidos.add(new JLabel(String.valueOf(agente.obtenerubicacion().getLat())));
			agentesConocidos.add(new JLabel(String.valueOf(agente.obtenerubicacion().getLon())));
			JButton btnAgregar = new JButton("Agregar");
			btnAgregar.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					agregarAgente(agente);
					ocultarEstadisticas();
				}
			});
			

			JButton btnEliminar = new JButton("Eliminar");
			btnEliminar.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					eliminarAgente(agente);
					ocultarEstadisticas();
				}
			});
			
			JButton btnOlvidar = new JButton("Olvidar");
			btnOlvidar.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					olvidarAgente(agente);
					ocultarEstadisticas();
				}
			});
			
			agentesConocidos.add(btnAgregar);
			agentesConocidos.add(btnEliminar);
			agentesConocidos.add(btnOlvidar);
		}
		
		_frame.getContentPane().add(agentesConocidos);
	}
	
	private void agregarAgente(Coordinate ubicacion)
	{
		try
		{
			String nombre = JOptionPane.showInputDialog("Ingrese el nombre clave: ");
			if(nombre != null)
			{
				_controller.agregarNuevoAgente(nombre, ubicacion);
				MapMarkerDot mapMarkerDot = new MapMarkerDot(_controller.obtenerUltimoAgente().obtenerNombre(), ubicacion);
				mapMarkerDot.setBackColor(Color.RED);
				_mapa.addMapMarker(mapMarkerDot);
				actualizarAgentesConocidos();
				mostrarArcos();
			}
		} 
		catch(IllegalArgumentException e)
		{
			agregarAgente(ubicacion);
		}
	}


	private void agregarAgente(Agente agente) {
		_controller.agregarAgenteConocido(agente);
		MapMarkerDot mapMarkerDot = new MapMarkerDot(agente.obtenerNombre(), agente.obtenerubicacion());
		mapMarkerDot.setBackColor(Color.RED);
		_mapa.addMapMarker(mapMarkerDot);
		mostrarArcos();
	}
	
	private void eliminarAgente(Agente agente) {
		_controller.eliminarAgente(agente);
		
		updateMapMarkers();
		
		mostrarArcos();
	}
	
	private void olvidarAgente(Agente agente) {
		_controller.olvidarAgente(agente);

		updateMapMarkers();
		
		mostrarArcos();
		
		actualizarAgentesConocidos();
	}
	
	private void updateMapMarkers() {
		
		_mapa.removeAllMapMarkers();
		
		for(Agente agente : _controller.obtenerAgentesEnMapa()) {
			MapMarkerDot mapMarkerDot = new MapMarkerDot(agente.obtenerNombre(), agente.obtenerubicacion());
			mapMarkerDot.setBackColor(Color.RED);
			_mapa.addMapMarker(mapMarkerDot);
		}
	}
	
	private void actualizarAgentesConocidos() {
		agentesConocidos.setVisible(false);
		_frame.remove(agentesConocidos);
		
		inicializarAgentesConcidos();
	}
	
	private void mostrarArcos()
	{
		ArrayList<Arco> arcos = _controller.obtenerArcos();
		_mapa.removeAllMapPolygons();
		for(Arco a: arcos)
		{
			Coordinate agenteOrigen = _controller.obtenerAgente(a.obtenerPosicionX()).obtenerubicacion();
			Coordinate agenteDestino = _controller.obtenerAgente(a.obtenerPosicionY()).obtenerubicacion();
			MapPolygonImpl camino = new MapPolygonImpl("" + a.obtenerPeso(), agenteOrigen, agenteDestino, agenteOrigen);
			_mapa.addMapPolygon(camino);
		}
	}
	
	private void actulizarEstadisticas()
	{
		_costo_promedio.setText("Costo promedio   " + _controller.obtenerCostoPromedio());
		_peso_total.setText("Peso Total   " + _controller.obtenerPesoTotal());
		_desviacion_estandar.setText("Desviacion estandar   " + _controller.obtenerDesviacionEstandar());
	}
	
	private void ocultarEstadisticas()
	{
		_costo_promedio.setText("Costo promedio   ");
		_peso_total.setText("Peso Total   ");
		_desviacion_estandar.setText("Desviacion estandar   ");	
	}
}
