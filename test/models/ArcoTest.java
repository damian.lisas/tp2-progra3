package models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ArcoTest 
{

	private Arco _arco;
	
	@Test
	public void posicionXtest() 
	{
		_arco = new Arco(5, 1, 20);
		assertTrue(_arco.obtenerPosicionX() == 5);
	}
	
	@Test
	public void posicionYTest()
	{
		_arco = new Arco(5, 1, 20);
		assertTrue(_arco.obtenerPosicionY() == 1);
	}
	
	@Test
	public void pesoTest()
	{
		_arco = new Arco(5, 1, 20);
		assertTrue(_arco.obtenerPeso() == 20);
	}
	
	@Test
	public void toStringTest()
	{
		_arco = new Arco(5, 1, 20);
		assertEquals("(5, 1, 20)", _arco.toString());
	}
	
	@Test
	@DisplayName(value = "Arcos simetricos")
	public void esSimetricoTest() {
		Arco arcoIda = new Arco(10, 20, 25);
		Arco arcoVuelta = new Arco(20, 10, 25);
		
		assertTrue(true == arcoIda.esSimetrico(arcoVuelta));
	}
	

	@Test
	@DisplayName(value = "Arcos no simetricos")
	public void noEsSimetricoTest() {
		Arco arcoIda = new Arco(10, 20, 25);
		Arco arcoVuelta = new Arco(30, 10, 25);
		
		assertTrue(false == arcoIda.esSimetrico(arcoVuelta));
	}

}
