package controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

import controllers.MainController;
import models.Agente;

class MainControllerTest 
{
	private static MainController _controller;
	
	@BeforeEach
	public void setup() {
		_controller = MainController.createTestController();
	}
	
	@AfterEach
	public void cleanDatabase() {
		_controller.eliminarTodosLosAgentes();
	}
	
	@Test
	public void agregarAgenteTest() 
	{
		_controller.agregarNuevoAgente("Shask", new Coordinate(0,0));
		assertEquals(_controller.obtenerAgente(0).obtenerNombre(), "Shask");
	}
	
	@Test
	@DisplayName(value = "Olvidar agente")
	public void olvidarAgenteTest() {
		Agente agente = _controller.agregarNuevoAgente("Zorro 1", new Coordinate(0, 1));
		
		_controller.olvidarAgente(agente);
		
		assertTrue(_controller.obtenerAgentesConocidos().size() == 0);
	}
	
	@Test
	public void generarGrafoCompletoTest()
	{
		_controller.agregarNuevoAgente("Shask", new Coordinate(0,0));
		_controller.agregarNuevoAgente("Kufha", new Coordinate(1,1));
		_controller.agregarNuevoAgente("esmeralda", new Coordinate(2,2));
		_controller.agregarNuevoAgente("Shask", new Coordinate(3,3));
		assertTrue(_controller.obtenerArcos().size() == 6);
	}
	
	@Test
	public void generarAgmTest()
	{
		_controller.agregarNuevoAgente("Shask", new Coordinate(0,0));
		_controller.agregarNuevoAgente("Esmeralda", new Coordinate(1,1));
		_controller.agregarNuevoAgente("Moskov", new Coordinate(2,2));
		_controller.agregarNuevoAgente("Guinevere", new Coordinate(3,3));
		_controller.generarAgm();
		assertTrue(_controller.obtenerArcos().size() == 3);
	}
	
	@Test
	public void obtenerUltimoAgenteTest()
	{
		_controller.agregarNuevoAgente("Shask", new Coordinate(0,0));
		_controller.agregarNuevoAgente("Esmeralda", new Coordinate(1,1));
		_controller.agregarNuevoAgente("Moskov", new Coordinate(2,2));
		_controller.agregarNuevoAgente("Guinevere", new Coordinate(3,3));
		assertEquals(_controller.obtenerUltimoAgente().obtenerNombre(), "Guinevere");
	}
	
	@Test
	public void eliminarAgenteTest()
	{
		_controller.agregarNuevoAgente("Shask", new Coordinate(0,0));
		_controller.agregarNuevoAgente("Esmeralda", new Coordinate(1,1));
		_controller.agregarNuevoAgente("Moskov", new Coordinate(2,2));
		_controller.agregarNuevoAgente("Guinevere", new Coordinate(3,3));
		_controller.eliminarAgente(_controller.obtenerAgente(1));
		assertEquals(_controller.obtenerAgente(1).obtenerNombre(), "Moskov");
	}
}
