package logic;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;

class GrafoPonderadoTest 
{
	private GrafoPonderado _grafo;

	@Test
	public void verticesNegativosTest() 
	{
		assertThrows(IllegalArgumentException.class, () -> _grafo = new GrafoPonderado(-1));
	}
	
	@Test
	public void verificarPesoValidoTest()
	{
		_grafo = new GrafoPonderado(5);
		assertThrows(IllegalArgumentException.class, () -> _grafo.agregarArco(2, 2, -1));
	}
	
	@Test
	public void verificarPosicionXNegativaTest()
	{
		_grafo = new GrafoPonderado(5);
		assertThrows(IndexOutOfBoundsException.class, () -> _grafo.agregarArco(-1, 2, 5));
	}

	@Test
	public void verificarPosicionXTest()
	{
		_grafo = new GrafoPonderado(5);
		assertThrows(IndexOutOfBoundsException.class, () -> _grafo.agregarArco(6, 2, 5));
	}
	
	@Test
	public void verificarPosicionYNegativaTest()
	{
		_grafo = new GrafoPonderado(5);
		assertThrows(IndexOutOfBoundsException.class, () -> _grafo.agregarArco(2, -1, 5));
	}

	@Test
	public void verificarPosicionYTest()
	{
		_grafo = new GrafoPonderado(5);
		assertThrows(IndexOutOfBoundsException.class, () -> _grafo.agregarArco(2, 6, 5));
	}
	
	@Test
	public void verificarAristaTest()
	{
		_grafo = new GrafoPonderado(5);
		assertThrows(IllegalArgumentException.class, () -> _grafo.agregarArco(1, 1, 5));
	}
	
	@Test
	public void verificarVerticeObtenerVecinosTest()
	{
		_grafo = new GrafoPonderado(5);
		assertThrows(IllegalArgumentException.class, () -> _grafo.obtenerVecinos(5));
	}
	
	@Test
	public void agergarAristaTest()
	{
		_grafo = new GrafoPonderado(5);
		_grafo.agregarArco(1, 2, 50);
		assertEquals(_grafo.obtenerPeso(1, 2), 50);
	}
	
	@Test
	public void agergarAristaInversaTest()
	{
		_grafo = new GrafoPonderado(5);
		_grafo.agregarArco(1, 2, 50);
		assertEquals(_grafo.obtenerPeso(2, 1), 50);
	}
	
	@Test
	public void obtenerTamañoTest()
	{
		_grafo = new GrafoPonderado(5);
		assertEquals(_grafo.obtenerTamaño(), 5);
	}
	
	@Test
	public void agregarVerticeTest()
	{
		_grafo = new GrafoPonderado(5);
		_grafo.agregarArco(0, 1, 5);
		_grafo.agregarArco(4, 3, 100);
		_grafo.agergarVertice();
		assertTrue(_grafo.obtenerPeso(3, 4) == 100 && _grafo.obtenerPeso(1, 0) == 5);
	}
	
	@Test
	public void agregarVerticeTest2()
	{
		_grafo = new GrafoPonderado(5);
		_grafo.agergarVertice();
		assertTrue(_grafo.obtenerTamaño() == 6);
	}
	
	@Test
	public void obtenerPesoTotalTest()
	{
		_grafo = new GrafoPonderado(5);
		_grafo.agregarArco(0, 1, 5);
		_grafo.agregarArco(1, 2, 1);
		_grafo.agregarArco(2, 3, 150);
		_grafo.agregarArco(3, 4, 6);
		_grafo.agregarArco(4, 0, 26);
		assertTrue(_grafo.getPesoTotal() == 188);
	}
	
	@Test
	public void obtenerPesoTotalDeAgmTest()
	{
		_grafo = new GrafoPonderado(4);
		_grafo.agregarArco(0, 1, 5);
		_grafo.agregarArco(0, 2, 1);
		_grafo.agregarArco(1, 3, 150);
		_grafo.agregarArco(2, 3, 6);
		_grafo = Primm.generarAgm(_grafo);
		assertTrue(_grafo.getPesoTotal() == 12);
	}
	
	@Test
	public void eliminarVerticeTest()
	{
		_grafo = new GrafoPonderado(4);
		_grafo.agregarArco(0, 1, 5);
		_grafo.agregarArco(0, 2, 1);
		_grafo.agregarArco(1, 3, 150);
		_grafo.agregarArco(2, 3, 6);
		_grafo.eliminarVertice(3);
		assertTrue(_grafo.obtenerTamaño() == 3);
	}
	
	@Test
	public void eliminarArcosTest()
	{
		_grafo = new GrafoPonderado(4);
		_grafo.agregarArco(0, 1, 5);
		_grafo.agregarArco(0, 2, 1);
		_grafo.agregarArco(1, 3, 150);
		_grafo.agregarArco(2, 3, 6);
		_grafo.eliminarVertice(3);
		assertTrue(_grafo.obtenerArcos().size() == 3);
	}
	
	@Test
	public void obtenerCostoPromedioTest()
	{
		_grafo = new GrafoPonderado(4);
		_grafo.agregarArco(0, 1, 5);
		_grafo.agregarArco(0, 2, 1);
		_grafo.agregarArco(1, 3, 150);
		_grafo.agregarArco(2, 3, 6);
		assertTrue(_grafo.obtenerCostoPromedio() == 40.5);
	}
}
