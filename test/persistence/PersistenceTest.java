package persistence;


import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.openstreetmap.gui.jmapviewer.Coordinate;

import models.Agente;

class PersistenceTest {
	
	private static PersistenceFacade facade;
	private static Agente agente;
	
	@BeforeAll
	public static void setupOne() {
		facade = PersistenceFacade.createTestFacade();
	}
	
	@BeforeEach
	public void setup(TestInfo testInfo) {
		System.out.println("Testeando: " + testInfo.getDisplayName());
		
		agente = new Agente("Agente", new Coordinate(10.0, 20.0), 1);
		
	}
	
	@AfterEach
	@DisplayName(value = "Eliminar agentes de prueba de la base de datos")
	public void cleanDatabase() {
		facade.elimarTodos();
	}
	
	@Test
	@DisplayName(value = "Crear un nuevo agente")
	public void guardarAgenteTest() {
		assertTrue(facade.guardarAgente(agente));
		
		Agente agenteCreado = facade.obtenerAgente(agente.getUuid());
		
		assertTrue(agenteCreado.getUuid().equals(agente.getUuid()));
	}
	
	@Test
	@DisplayName(value = "Crear un agente existente")
	public void guardarAgenteRepetid() {
		facade.guardarAgente(agente);
		
		assertTrue(false == facade.guardarAgente(agente));
	}
	
	@Test
	@DisplayName(value = "Obtener todos los agentes")
	public void obtenerTodos() {
		Agente agente1 = new Agente("Zorro 1", new Coordinate(10.0, 120.1), 1);
		Agente agente2 = new Agente("Zorro 2", new Coordinate(20.0, 130.1), 2);
		Agente agente3 = new Agente("Zorro 1", new Coordinate(30.0, 140.1), 3);
		
		facade.guardarAgente(agente1);
		facade.guardarAgente(agente2);
		facade.guardarAgente(agente3);
		
		List<Agente> agentes = facade.obtenerTodosLosAgentes();
		
		assertTrue(agentes.size() >= 3);
	}
	
	@Test
	@DisplayName(value = "Actualizar un agente")
	public void actualizarAgente() {
		facade.guardarAgente(agente);
		Agente estadoInicial = facade.obtenerAgente(agente.getUuid());
		
		agente.setNombre("Zorro 1");
		facade.actualizarAgente(agente);
		
		assertTrue(false == (estadoInicial.compareTo(agente) == 0));
	}
	
	@Test
	@DisplayName(value = "Actualizar agente inexistente")
	public void actualizarAgenteInexistente() {
		Agente agente = new Agente("Zorro 2", new Coordinate(0.0, 0.1), 2);
		
		assertTrue(false == facade.actualizarAgente(agente));
	}
	
	@Test
	@DisplayName(value = "Eliminar un agente")
	public void eliminarAgente() {
		facade.guardarAgente(agente);
		
		assertTrue(true == facade.eliminarAgente(agente));
		assertTrue(null == facade.obtenerAgente(agente.getUuid()));
	}

}
